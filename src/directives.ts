import ClickOutside from "./directives/ClickOutside";
import { App } from "vue";
export default function (app: App) {
  ClickOutside(app);
}
