export type FormData = {
  name: string;
  description: string;
  fields: FormField[];
  password: string | null;
  passwordEnabled: boolean;
  screenSaver: {
    enabled: boolean;
    title: string;
    subTitle: string;
    appearAfter: number;
    buttonText: string;
    backgroundImage: string;
  };
  activeDates: {
    enabled: boolean;
    startDate: string;
    endDate: string;
  };
};

export type FormField = {
  name: string;
  type: string;
  required: boolean;
  options: { value: string; name: string }[];
  isEditable: boolean;
  regex: string;
  id: string;
};

export type User = {
  id: string;
  name: string;
  email: string;
  password: string;
  role: string;
};

export type Form = {
  uuid: string;
  data: FormData;
  submissions_count: number;
  created_at: string;
  updated_at: string;
};

export type Forms = {
  uuid: string;
  name: string;
  submissions_count: number;
  created_at: string;
  updated_at: string;
}[];
