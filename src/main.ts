import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import axios from 'axios';
import Directives from './directives';

import InputComponent from './components/form/InputComponent.vue';
import DropdownComponent from './components/form/DropdownComponent.vue';
import HeaderComponent from './components/HeaderComponent.vue';
import { User } from './types';

declare global {
  interface Window {
    axios: any;
    isAuthenticated: boolean;
    userData: User;
  }
}
declare module 'vue' {
  interface ComponentCustomProperties {
    window: Window;
  }
}
declare module 'vue-router' {
  interface RouteMeta {
    requiresAuth?: boolean;
    roles?: string[];
  }
}
// axios.post("/api/user", {
//   email: "david@ddcode.co",
//   password: "123456",
//   role: "admin",
//   name: "david",
// });

axios.defaults.withCredentials = true;

window.isAuthenticated = false;
router.beforeEach(function (to, from, next) {
  if (window.isAuthenticated) {
    if (to.meta.requiresAuth) {
      if (to.meta.roles?.includes(window.userData?.role || '')) {
        next();
      } else {
        next({ name: 'FormDashboard' });
      }
    } else {
      if (to.name === 'FormPreview') {
        next();
      } else {
        next({ name: 'FormDashboard' });
      }
    }
  } else {
    if (to.meta.requiresAuth === false) {
      next();
    } else {
      next({ name: 'LoginPage' });
    }
  }
});

const app = createApp(App);

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status === 401) {
      window.isAuthenticated = false;
      if (
        router.currentRoute &&
        router.currentRoute.value &&
        router.currentRoute.value.meta &&
        router.currentRoute.value.meta.requiresAuth
      ) {
        router.push({ name: 'LoginPage' });
      }
    }
    return Promise.reject(error);
  }
);

axios
  .get('/api/user/')
  .then((response) => {
    if (response.data) {
      window.isAuthenticated = true;
      window.userData = response.data;
    }
  })
  .finally(() => {
    Directives(app);
    app.config.globalProperties.window = window;

    app.component('InputComponent', InputComponent);
    app.component('HeaderComponent', HeaderComponent);
    app.component('DropdownComponent', DropdownComponent);

    app.use(router);
    app.mount('#app');
  });
