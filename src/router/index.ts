import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'LoginPage',
    component: () => import(/* webpackChunkName: "form" */ '../views/LoginPage.vue'),
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/',
    name: 'FormDashboard',
    component: () => import(/* webpackChunkName: "viewer" */ '../views/FormDashboard.vue'),
    meta: {
      requiresAuth: true,
      roles: ['superAdmin', 'admin', 'editor', 'user'],
    },
  },
  {
    path: '/form',
    name: 'FormCreate',
    component: () => import(/* webpackChunkName: "editor" */ '../views/FormCreator.vue'),
    meta: {
      requiresAuth: true,
      roles: ['superAdmin', 'admin', 'editor'],
    },
  },
  {
    path: '/form/:id',
    name: 'FormEdit',
    component: () => import(/* webpackChunkName: "editor" */ '../views/FormCreator.vue'),
    meta: {
      requiresAuth: true,
      roles: ['superAdmin', 'admin', 'editor'],
    },
  },
  {
    path: '/form-preview/:id',
    name: 'FormPreview',
    component: () => import(/* webpackChunkName: "form" */ '../views/FormPreview.vue'),
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/form-submissions/:id',
    name: 'FormSubmissions',
    component: () => import(/* webpackChunkName: "viewer" */ '../views/FormSubmissions.vue'),
    meta: {
      requiresAuth: true,
      roles: ['superAdmin', 'admin', 'editor', 'viewer'],
    },
  },
  {
    path: '/admin',
    name: 'AdminDashboard',
    component: () => import(/* webpackChunkName: "admin" */ '../views/AdminDashboard.vue'),
    meta: {
      requiresAuth: true,
      roles: ['superAdmin', 'admin'],
    },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
