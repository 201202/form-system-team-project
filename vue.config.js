const { defineConfig } = require('@vue/cli-service');
const path = require('path');
module.exports = defineConfig({
  transpileDependencies: true,
  chainWebpack: (config) => {
    config.resolve.alias.set('@', path.join(__dirname, '/src'));
    config.module
      .rule('vue')
      .use('vue-loader')
      .tap((options) => {
        return { ...options, reactivityTransform: true };
      });
  },
  css: {
    sourceMap: true,
  },

  devServer: {
    proxy: {
      '^/api': {
        target: 'https://timski.ddcode.co/',
        changeOrigin: true,
      },
    },
  },

  productionSourceMap: false,
});
