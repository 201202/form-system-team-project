# Form System Team Project

## Features

- Active form dates 
- Screen saver configuration
    - Title
    - Sub title text
    - Button text
    - Appear after
    - Background image
- Regex validation
- Customisation of forms 
- Generate specific forms with specific predifiend fields
- Export data in Excel or in JSON
